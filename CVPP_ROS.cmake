
################################################################
########## ROS

find_package(catkin REQUIRED COMPONENTS
        roscpp rospy
        std_msgs geometry_msgs
        image_transport cv_bridge
)

catkin_package(
)

include_directories(
        ${catkin_INCLUDE_DIRS}
)

link_libraries(
        ${catkin_LIBRARIES}
)

################################################################
########## CVPP

# Change to the CMAKE folder of CVPP
set( CMAKE_MODULE_PATH ${CMAKE_MODULE_PATH} "~/Documents/code/cpp/cvpp/cmake" )
include( CVPP )

################################################################
