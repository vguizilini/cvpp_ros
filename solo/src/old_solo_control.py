
from dronekit import connect , VehicleMode , LocationGlobal, LocationGlobalRelative
from pymavlink import mavutil
from threading import Thread
from cv_bridge import CvBridge

from std_msgs.msg import Int32 , Float32 , String , Bool , Empty
from geometry_msgs.msg import Vector3

import rospy , time , math , cv2 , sys

running = True

# Heading
def callback_heading( self , attr_name , data ):

    msg = Int32()
    msg.data = data

    pub_heading.publish( msg )

# Attitude
def callback_attitude( self , attr_name , data ):

    msg = Vector3()
    msg.x = data.roll
    msg.y = data.pitch
    msg.z = data.yaw

    pub_attitude.publish( msg )

# Velocity
def callback_velocity( self , attr_name , data ):

    msg = Vector3()
    msg.x = data[0]
    msg.y = data[1]
    msg.z = data[2]

    pub_velocity.publish( msg )

# GPS
def callback_gps( self , attr_name , data ):

    msg = Vector3()
    msg.x = data.fix_type
    msg.y = data.satellites_visible

    pub_gps.publish( msg )

# Location Global
def callback_location_global( self , attr_name , data ):

    msg = Vector3()
    msg.x = data.lat
    msg.y = data.lon
    msg.z = data.alt

    pub_location_global.publish( msg )

# Location Relative
def callback_location_relative( self , attr_name , data ):

    msg = Vector3()
    msg.x = data.lat
    msg.y = data.lon
    msg.z = data.alt

    pub_location_relative.publish( msg )

# Location Local
def callback_location_local( self , attr_name , data ):

    msg = Vector3()
    msg.x = data.north
    msg.y = data.east
    msg.z = data.down

    pub_location_local.publish( msg )

# Mode
def callback_mode( self , attr_name , data ):

    msg = String()
    msg.data = data.name

    pub_mode.publish( msg )

# Status
def callback_status( self , attr_name , data ):

    msg = String()
    msg.data = data.state

    pub_status.publish( msg )

# Armed
def callback_armed( self , attr_name , data ):

    msg = Bool()
    msg.data = data

    pub_armed.publish( msg )

# Armable
def callback_armable( self , attr_name , data ):

    msg = Bool()
    msg.data = data

    pub_armable.publish( msg )

# Battery
def callback_battery( self , attr_name , data ):

    msg = Int32()
    msg.data = data.level

    pub_battery.publish( msg )

# REQUEST INFO
def request_info( data ):

    if data.data == "VEHICLE_MODE":

        msg = String()
        msg.data = vehicle.mode.name
        pub_mode.publish( msg )

    elif data.data == "SYSTEM_STATUS":

        msg = String()
        msg.data = vehicle.system_status.state
        pub_status.publish( msg )

    elif data.data == "ARMED_FLAG":

        msg = Bool()
        msg.data = vehicle.armed
        pub_armed.publish( msg )

    elif data.data == "ARMABLE_FLAG":

        msg = Bool()
        msg.data = vehicle.is_armable
        pub_armable.publish( msg )

    elif data.data == "GPS_LOCK":

        msg = Vector3()
        msg.x = vehicle.gps_0.fix_type
        msg.y = vehicle.gps_0.satellites_visible
        pub_gps.publish( msg )

    elif data.data == "MODE GUIDED":

        vehicle.mode = VehicleMode( "GUIDED" )

    elif data.data == "MODE LOITER":

        vehicle.mode = VehicleMode( "LOITER" )

# COMMAND_TAKEOFF
def command_takeoff( data ):

    while not vehicle.is_armable:
        time.sleep(1)

    vehicle.mode = VehicleMode( "GUIDED" )
    vehicle.armed = True

    while not vehicle.armed:
        time.sleep(1)
    vehicle.simple_takeoff( 2.0 )

# COMMAND_LANDING
def command_landing( data ):

    vehicle.mode = VehicleMode( "LAND" )

# COMMAND_LOCATION
def command_location( data ):

    msg = vehicle.message_factory.set_position_target_local_ned_encode(
        0 , 0 , 0 , mavutil.mavlink.MAV_FRAME_LOCAL_NED ,
        0b0000111111111000, data.x , - data.y , - data.z , 0 , 0 , 0 , 0 , 0 , 0 , 0 , 0 )
    vehicle.send_mavlink( msg )

# COMMAND_VELOCITY
def command_velocity( data ):

    msg = vehicle.message_factory.set_position_target_local_ned_encode(
        0 , 0 , 0 , mavutil.mavlink.MAV_FRAME_LOCAL_NED ,
        0b0000111111000111, 0 , 0 , 0 , data.x , data.y , data.z , 0 , 0 , 0 , 0 , 0 )
    vehicle.send_mavlink( msg )

# COMMAND_BEARING
def command_bearing( data ):

    msg = vehicle.message_factory.command_long_encode(
        0 , 0 , mavutil.mavlink.MAV_CMD_CONDITION_YAW , 0,
        data.data , 0 , 1 , 0 , 0 , 0 , 0 )
    vehicle.send_mavlink( msg )

# RUN
def run():

    global running

    vehicle.add_attribute_listener( "heading"  , callback_heading  )
    vehicle.add_attribute_listener( "attitude" , callback_attitude )
    vehicle.add_attribute_listener( "velocity" , callback_velocity )

    vehicle.add_attribute_listener( "location.global_frame"          , callback_location_global   )
    vehicle.add_attribute_listener( "location.global_relative_frame" , callback_location_relative )
    vehicle.add_attribute_listener( "location.local_frame"           , callback_location_local    )

    vehicle.add_attribute_listener( "mode"          , callback_mode    )
    vehicle.add_attribute_listener( "system_status" , callback_status  )
    vehicle.add_attribute_listener( "armed"         , callback_armed   )
    vehicle.add_attribute_listener( "is_armable"    , callback_armable )
    vehicle.add_attribute_listener( "battery"       , callback_battery )
    vehicle.add_attribute_listener( "gps_0"         , callback_gps     )

    rospy.Subscriber( "solo/command/request" , String , request_info )

    rospy.Subscriber( "solo/command/takeoff" , Empty , command_takeoff )
    rospy.Subscriber( "solo/command/landing" , Empty , command_landing )

    rospy.Subscriber( "solo/command/location" , Vector3 , command_location )
    rospy.Subscriber( "solo/command/velocity" , Vector3 , command_velocity )
    rospy.Subscriber( "solo/command/bearing"  , Float32 , command_bearing  )

    rospy.spin()
    running = False

# SITL

print "Starting copter simulator (SITL)"
from dronekit_sitl import SITL
sitl = SITL()
sitl.download('copter', '3.3', verbose=True)
sitl_args = ['-I0', '--model', 'quad', '--home=-35.363261,149.165230,584,353']
sitl.launch(sitl_args, await_ready=True, restart=True)
str = 'tcp:127.0.0.1:5760'

# SOLO

#str = "udpout:10.1.1.10:14560"

# MAIN

print "Connecting to Solo ..." , str
vehicle = connect( str , wait_ready = True , rate = 10 )

rospy.init_node( "ROS_Python" , anonymous = True )

pub_heading  = rospy.Publisher( "solo/state/heading"  , Int32   , queue_size = 0 )
pub_attitude = rospy.Publisher( "solo/state/attitude" , Vector3 , queue_size = 0 )
pub_velocity = rospy.Publisher( "solo/state/velocity" , Vector3 , queue_size = 0 )

pub_location_global   = rospy.Publisher( "solo/location/global"   , Vector3 , queue_size = 0 )
pub_location_relative = rospy.Publisher( "solo/location/relative" , Vector3 , queue_size = 0 )
pub_location_local    = rospy.Publisher( "solo/location/local"    , Vector3 , queue_size = 0 )

pub_mode    = rospy.Publisher( "solo/info/mode"    , String  , queue_size = 10 )
pub_status  = rospy.Publisher( "solo/info/status"  , String  , queue_size = 10 )
pub_armed   = rospy.Publisher( "solo/info/armed"   , Bool    , queue_size = 10 )
pub_armable = rospy.Publisher( "solo/info/armable" , Bool    , queue_size = 10 )
pub_battery = rospy.Publisher( "solo/info/battery" , Int32   , queue_size = 10 )
pub_gps     = rospy.Publisher( "solo/info/gps"     , Vector3 , queue_size = 10 )

run()



