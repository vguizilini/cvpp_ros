#include "./solo.h"

namespace cvpp
{

// Run Draw
const void
Solo::runDraw()
{
    CPPlot draw( "Window" , 1 , 2 );
    draw[0].set3Dworld().setViewer( -5.0 , 0.0 , 5.0 );
    draw[1].set2Dimage().setResolution( 480 , 640 );

    gdrone = new Drone( 1.0 );
    ldrone = new Drone( 1.0 );

    new_img = false;
    unsigned tex = draw.addTexture3U( 480 , 640 );

    Thread threadData = Thread( &Solo::runData , this );

    while( running && draw.input() )
    {
        draw[0].clear();

        draw.object( *ldrone );

        draw.lwc(2,GRE).line3D( lpath );

        if( !goals.empty() )
        {
            draw.psc(8,CYA).pt3D( goals[cnt] );
            for( int i = 0 ; i < cnt ; ++i )
                draw.psc(6,BLU).pt3D( goals[i] );
            for( int i = cnt + 1 ; i < goals.n() ; ++i )
                draw.psc(6,MAG).pt3D( goals[i] );
            draw.lwc(2,CYA).line3D( goals[-1] , target );
            draw.lwc(2,BLU).line3D( goals );
            draw.psc(6,RED).pt3D( target );
        }

        drawText( draw , *ldrone );

        draw[1].clear();

        if( new_img ) draw.updTexture( tex , img );
        if( img.filled() ) draw.useTexture( tex );

        draw.updateWindow(30);

        if( draw.keys.space )
        {
            if( !flying )
                 node->publishEmpty( pub_takeoff );
            else node->publishEmpty( pub_landing );
            flying = !flying; recording = true; halt(100);
        }

        if( draw.keys.enter )
        {
            goals.push( target );
            halt(100);
        }

        if( draw.keys.l )
        {
            addMission();
            halt(100);
        }

        if( draw.keys.z )
        {
            node->publishString( pub_request , "MODE LOITER" );
            halt(100);
        }

        if( draw.keys.x )
        {
            node->publishString( pub_request , "MODE GUIDED" );
            halt(100);
        }

        if( draw.keys.tab )
        {
            control = !control;
            halt(100);
        }

        if( draw.keys.rshift && cnt < goals.n() - 1 )
        {
            publishNextGoal();
            halt(100);
        }

        if( goals.empty() && draw.keys.udlr() )
        {
            goals.push( ldrone->pose->getPosPt() );
            target = goals[0];
        }

        if( draw.keys.up    ) target.x += 0.1;
        if( draw.keys.down  ) target.x -= 0.1;
        if( draw.keys.left  ) target.y += 0.1;
        if( draw.keys.right ) target.y -= 0.1;
    }

    running = false;
    threadData.join();

    delete gdrone;
    delete ldrone;
}

const void
Solo::drawText( CPPlot& draw , Drone& drone )
{
    Scalar clr;

    draw.flatten();

    // GLOBAL

    clr = gfirst ? RED : YEL ;

    draw.clr(clr).text2D( 0.01 , 0.95 , "Latitude :" );
    draw.clr(WHI).text2D( 0.15 , 0.95 , std::to_string( gpos.x ) );

    draw.clr(clr).text2D( 0.01 , 0.90 , "Longitude :" );
    draw.clr(WHI).text2D( 0.15 , 0.90 , std::to_string( gpos.y ) );

    draw.clr(clr).text2D( 0.01 , 0.85 , "Altitude :" );
    draw.clr(WHI).text2D( 0.15 , 0.85 , std::to_string( gpos.z ) );

    // LOCAL

    clr = lfirst ? RED : YEL ;

    draw.clr(clr).text2D( 0.02 , 0.70 , "X :" );
    draw.clr(WHI).text2D( 0.10 , 0.70 , std::to_string( drone.pose->x() ) );
    draw.clr(CYA).text2D( 0.22 , 0.70 , "( " + std::to_string( target.x ) + " )" );

    draw.clr(clr).text2D( 0.02 , 0.65 , "Y :" );
    draw.clr(WHI).text2D( 0.10 , 0.65 , std::to_string( drone.pose->y() ) );
    draw.clr(CYA).text2D( 0.22 , 0.65 , "( " + std::to_string( target.y ) + " )" );

    draw.clr(clr).text2D( 0.02 , 0.60 , "Z :" );
    draw.clr(WHI).text2D( 0.10 , 0.60 , std::to_string( drone.pose->z() ) );
    draw.clr(CYA).text2D( 0.22 , 0.60 , "( " + std::to_string( target.z ) + " )" );

    // ATTITUDE

    clr = afirst ? RED : YEL ;

    draw.clr(clr).text2D( 0.02 , 0.55 , "R :" );
    draw.clr(WHI).text2D( 0.10 , 0.55 , std::to_string( drone.pose->r() ) );

    draw.clr(clr).text2D( 0.02 , 0.50 , "P :" );
    draw.clr(WHI).text2D( 0.10 , 0.50 , std::to_string( drone.pose->p() ) );

    draw.clr(clr).text2D( 0.02 , 0.45 , "W :" );
    draw.clr(WHI).text2D( 0.10 , 0.45 , std::to_string( drone.pose->w() ) );

    // STATUS

    draw.clr(YEL).text2D( 0.01 , 0.03 , "STATUS :" );
    draw.clr(GRE).text2D( 0.15 , 0.03 , status );

    // MODE

    draw.clr(YEL).text2D( 0.31 , 0.03 , "MODE :" );
    draw.clr(GRE).text2D( 0.45 , 0.03 , mode );

    // BATTERY

    clr = battery > 70 ? GRE : battery > 40 ? CYA : RED ;

    draw.clr(YEL).text2D( 0.01 , 0.08 , "BATTERY :" );
    draw.clr(clr).text2D( 0.17 , 0.08 , std::to_string( battery ) + " %" );

    // ARMED

    clr = armable ? GRE : RED ;

    draw.clr(YEL).text2D( 0.61 , 0.03 , "ARMED :" );
    draw.clr(clr).text2D( 0.75 , 0.03 , armed ? "TRUE" : "FALSE" );

    // GPS_LOCK

    clr = gps.x < 2 ? RED : gps.x == 2 ? CYA : GRE ;

    draw.clr(YEL).text2D( 0.31 , 0.08 , "GPS LOCK :" );
    draw.clr(clr).text2D( 0.47 , 0.08 , std::to_string( (int)gps.y ) );

    // CONTROL

    draw.clr(YEL).text2D( 0.78 , 0.95 , "CONTROL TYPE" );
    draw.clr(GRE).text2D( 0.81 , 0.90 , control == LOCATION ? "LOCATION" : "VELOCITY" );

    // PATH

    draw.clr(YEL).text2D( 0.01 , 0.15 , "NUM. POSES :" );
    draw.clr(WHI).text2D( 0.20 , 0.15 , std::to_string( lpath.n() ) );

    // TARGETS

    draw.clr(YEL).text2D( 0.31 , 0.15 , "NUM. TARGETS :" );
    draw.clr(WHI).text2D( 0.55 , 0.15 , std::to_string( cnt ) + " / " +
                                        std::to_string( std::max( 0 , (int)goals.n() - 1 ) ) );

}

}

