#include "./solo.h"

namespace cvpp
{

// Run Data
const void
Solo::runData()
{
    node = new ROSnode( "Telemetry" );

    startSubscribers();
    startPublishers();

    cnt = 0;
    while( node->ok() && running )
    {
        ros::spinOnce();

        if( mode == "----" );
        {
            requestUpdate();
            halt(100);
        }

        if( goals.n() > 0 )
        {
            float d = dist( goals[cnt] , ldrone->pose->getPosPt() );
            if( d < 1.0 && cnt < goals.n() - 1 ) publishNextGoal();
        }
    }

    delete node;
}

const void
Solo::startSubscribers()
{
    node->addSubscriber( "solo/location/global" , &Solo::callbackGlobal   , this );
    node->addSubscriber( "solo/location/local"  , &Solo::callbackLocal    , this );
    node->addSubscriber( "solo/state/attitude"  , &Solo::callbackAttitude , this );

    node->addSubscriber( "solo/info/mode"    , &Solo::callbackMode    , this );
    node->addSubscriber( "solo/info/status"  , &Solo::callbackStatus  , this );
    node->addSubscriber( "solo/info/armed"   , &Solo::callbackArmed   , this );
    node->addSubscriber( "solo/info/armable" , &Solo::callbackArmable , this );
    node->addSubscriber( "solo/info/battery" , &Solo::callbackBattery , this );
    node->addSubscriber( "solo/info/gps"     , &Solo::callbackGPS     , this );

    node->addImageSubscriber( "solo/image" , &Solo::callbackImage , this );
}

const void
Solo::startPublishers()
{
    pub_location = node->addPublisher< msgVector3 >( "solo/command/location" );

    pub_takeoff  = node->addPublisher< msgEmpty >( "solo/command/takeoff" );
    pub_landing = node->addPublisher<  msgEmpty >( "solo/command/landing" );

    pub_request  = node->addPublisher< msgString  >( "solo/command/request" );
}

const void
Solo::requestUpdate()
{
    node->publishString( pub_request , "SYSTEM_STATUS" );
    node->publishString( pub_request , "VEHICLE_MODE"  );
    node->publishString( pub_request , "ARMED_FLAG"    );
    node->publishString( pub_request , "ARMABLE_FLAG"  );
    node->publishString( pub_request , "GPS_LOCK"      );
}

}

