#include "./solo.h"

#include "./solo_data.cpp"
#include "./solo_draw.cpp"
#include "./solo_callback.cpp"

namespace cvpp
{

Solo::Solo()
{
    gfirst = lfirst = afirst = true;
    status = mode = "----";

    gpath.reserve( 1e4 );
    lpath.reserve( 1e4 );
    goals.reserve( 100 );
}

Solo::~Solo()
{
}

const void
Solo::start()
{
    control = LOCATION;
    running = true; flying = recording = false;
    runDraw();
}

Pt3f
Solo::ned2lla( const Pt3f& mot ) const
{
    float rad = 6378137.0;

    float dLat = mot.x / ( rad );
    float dLon = mot.y / ( rad *std::cos( PI * gorg.x / 180.0 ) );
    float dAlt = mot.z;

    float nLat = gorg.x + ( dLat * 180.0 / PI );
    float nLon = gorg.y + ( dLon * 180.0 / PI );
    float nAlt = gorg.z + ( dAlt );

    return Pt3f( nLat , nLon , nAlt );
}


Pt3f
Solo::lla2ned( const Pt3f& goal ) const
{
    return Pt3f( ( goal.x - gorg.x ) * 1.113195e5 ,
                 ( goal.y - gorg.y ) * 1.113195e5 ,
                 ( goal.z - gorg.z ) );
}

const void
Solo::publishNextGoal()
{
    Pt3f tmp = ned2lla( goals[++cnt] );
    goal.x = tmp.x; goal.y = tmp.y; goal.z = tmp.z;
    node->publish( pub_location , goal );
}

const void
Solo::addMission()
{
    Pts3f pts( "data/solo_mission" ); pts.mat().c(2) += gorg.z;
    forLOOPi( pts.n() ) goals.push( lla2ned( pts[i] ) );
}

}
