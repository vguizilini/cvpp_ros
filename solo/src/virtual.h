#ifndef VIRTUAL_H
#define VIRTUAL_H

#include <cvpp/ros/ros_node.h>
#include <cvpp/interfaces/cpplot.h>
#include <cvpp/objects/object_drone.h>
#include <cvpp/objects/object_camera.h>
#include <cvpp/objects/object_board.h>

#include <ros/ros.h>

#include <std_msgs/Int32.h>
#include <std_msgs/Float32.h>
#include <std_msgs/String.h>
#include <std_msgs/Bool.h>
#include <std_msgs/Empty.h>
#include <geometry_msgs/Vector3.h>

#define LOCATION 0
#define VELOCITY 1

using msgInt32   = std_msgs::Int32;
using msgFloat32 = std_msgs::Float32;
using msgString  = std_msgs::String;
using msgBool    = std_msgs::Bool;
using msgEmpty   = std_msgs::Empty;
using msgVector3 = geometry_msgs::Vector3;

namespace cvpp
{

class Virtual
{

protected:

    ROSnode* node;
    CPPlot* draw;
    Drone* drone;

    msgInt32 heading , battery ;
    msgVector3 attitude , loc_global , loc_relative , loc_local , velocity , gps ;
    msgString mode , status ;
    msgBool armed , armable ;

    bool flag_heading , flag_attitude , flag_velocity ;
    bool flag_loc_global , flag_loc_relative , flag_loc_local ;
    bool flag_mode , flag_status , flag_armed , flag_armable , flag_battery , flag_gps ;

    bool started , new_state ;

    Mutex mut_heading , mut_attitude , mut_velocity ;
    Mutex mut_loc_global , mut_loc_relative , mut_loc_local ;
    Mutex mut_mode , mut_status , mut_armed , mut_armable , mut_battery , mut_gps ;

    unsigned pub_request ;
    unsigned pub_takeoff , pub_landing ;
    unsigned pub_location , pub_velocity , pub_bearing ;

    bool running ;
    Thread threadDraw , threadData ;

    msgVector3 goal , speed ;
    msgFloat32 angle ;

    unsigned cnt , control_type;
    Pt3f target ;

    Pts3f poses , goals ;
    Posef offset ;

public:

    Virtual();
    ~Virtual();

    const void start();
    const void startSubscribers();
    const void startPublishers();

    const void runDraw();
    const void runData();

    const void drawDrone();
    const void drawText();

    const void dataUpdate();
    const void stateUpdate();

    const unsigned isArmed() const { return armed.data; }
    const unsigned hasStarted() const { return started; }

    float distance() const;

    const void gotoLocation();

    const void controlLocation( CPPlot* );
    const void controlVelocity( CPPlot* );

    void headingCallback( const msgInt32::ConstPtr& msg ) // Heading
    { mut_heading.lock(); heading = *msg; mut_heading.unlock(); }
    void attitudeCallback( const msgVector3::ConstPtr& msg ) // Attitude
    { mut_attitude.lock(); attitude = *msg; mut_attitude.unlock(); new_state = true; }
    void velocityCallback( const msgVector3::ConstPtr& msg ) // Velocity
    { mut_velocity.lock(); velocity = *msg; mut_velocity.unlock(); }

    void locGlobalCallback( const msgVector3::ConstPtr& msg ) // Location Global
    { mut_loc_global.lock(); loc_global = *msg; mut_loc_global.unlock(); }
    void locRelativeCallback( const msgVector3::ConstPtr& msg ) // Location Relative
    { mut_loc_relative.lock(); loc_relative = *msg; mut_loc_relative.unlock(); }
    void locLocalCallback( const msgVector3::ConstPtr& msg ) // Location Local
    { mut_loc_local.lock(); loc_local = *msg; mut_loc_local.unlock(); new_state = true; }

    void modeCallback( const msgString::ConstPtr& msg ) // Mode
    { mut_mode.lock(); mode = *msg; mut_mode.unlock(); }
    void statusCallback( const msgString::ConstPtr& msg ) // Status
    { mut_status.lock(); status = *msg; mut_status.unlock(); }
    void armedCallback( const msgBool::ConstPtr& msg ) // Armed
    { mut_armed.lock(); armed = *msg; mut_armed.unlock(); }
    void armableCallback( const msgBool::ConstPtr& msg ) // Armable
    { mut_armable.lock(); armable = *msg; mut_armable.unlock(); }
    void batteryCallback( const msgInt32::ConstPtr& msg ) // Battery
    { mut_battery.lock(); battery = *msg; mut_battery.unlock(); }
    void gpsCallback( const msgVector3::ConstPtr& msg ) // GPS
    { mut_gps.lock(); gps = *msg; mut_gps.unlock(); }

};

}

#endif
