#include "./virtual.h"

namespace cvpp
{

Virtual::Virtual()
{
    flag_heading = flag_attitude = flag_velocity = true ;
    flag_loc_global = flag_loc_relative = flag_loc_local = true ;
    flag_mode = flag_status = flag_armed = flag_armable = flag_battery  = flag_gps = true ;

    mode.data = status.data = "NONE" ;
    battery.data = gps.x = gps.y = 0;

    poses.reserve( 1e4 );
    started = false;

    cnt = 0;
    goals.push( Pt3d( 0.0 , 0.0 , 0.0 ) );

    control_type = LOCATION;
}

Virtual::~Virtual()
{
}

float
Virtual::distance() const
{
    return sqrt( pow( drone->pose->x() - goals[cnt].x , 2.0 ) +
                 pow( drone->pose->y() - goals[cnt].y , 2.0 ) +
                 pow( drone->pose->z() - goals[cnt].z , 2.0 ) );
}

const void
Virtual::gotoLocation()
{
    cnt++;

    goal.x = goals[cnt].x;
    goal.y = goals[cnt].y;
    goal.z = goals[cnt].z;

    node->publish( pub_location , goal );
}

const void
Virtual::controlLocation( CPPlot* draw )
{
    if( draw->keys.up    ) target.x += 0.1;
    if( draw->keys.down  ) target.x -= 0.1;
    if( draw->keys.left  ) target.y += 0.1;
    if( draw->keys.right ) target.y -= 0.1;
    if( draw->keys.w     ) target.z += 0.1;
    if( draw->keys.s     ) target.z -= 0.1;

    if( goals.n() == 1 )
        goals[0] = drone->pose->getPosPt();

    if( distance() < 2.0 && cnt < goals.n() - 1 )
        gotoLocation();

    if( draw->keys.enter )
    {
        goals.push( target );
        if( distance() < 2.0 && cnt == goals.n() - 1 )
            gotoLocation();
        halt(100);
    }
}

const void
Virtual::controlVelocity( CPPlot* draw )
{
    speed.x = speed.y = speed.z = 0;
    angle.data = heading.data;

    if( draw->keys.up    ) speed.x += 0.5;
    if( draw->keys.down  ) speed.x -= 0.5;
    if( draw->keys.right ) speed.y += 0.5;
    if( draw->keys.left  ) speed.y -= 0.5;
    if( draw->keys.s     ) speed.z += 0.5;
    if( draw->keys.w     ) speed.z -= 0.5;

    float w = - drone->pose->w();
    float x = speed.x * cos(w) - speed.y * sin(w);
    float y = speed.x * sin(w) + speed.y * cos(w);
    speed.x = x; speed.y = y;

    node->publish( pub_velocity , speed );

    if( draw->keys.d ) angle.data += 5; if( angle.data > 360 ) angle.data -= 360 ;
    if( draw->keys.a ) angle.data -= 5; if( angle.data <   0 ) angle.data += 360 ;

    node->publish( pub_bearing , angle );
}

const void
Virtual::startSubscribers()
{
    if( flag_heading  ) node->addSubscriber( "solo/state/heading"  , &Virtual::headingCallback  , this );
    if( flag_attitude ) node->addSubscriber( "solo/state/attitude" , &Virtual::attitudeCallback , this );
    if( flag_velocity ) node->addSubscriber( "solo/state/velocity" , &Virtual::velocityCallback , this );

    if( flag_loc_global )   node->addSubscriber( "solo/location/global"   , &Virtual::locGlobalCallback   , this );
    if( flag_loc_relative ) node->addSubscriber( "solo/location/relative" , &Virtual::locRelativeCallback , this );
    if( flag_loc_local )    node->addSubscriber( "solo/location/local"    , &Virtual::locLocalCallback    , this );

    if( flag_mode    ) node->addSubscriber( "solo/info/mode"    , &Virtual::modeCallback    , this );
    if( flag_status  ) node->addSubscriber( "solo/info/status"  , &Virtual::statusCallback  , this );
    if( flag_armed   ) node->addSubscriber( "solo/info/armed"   , &Virtual::armedCallback   , this );
    if( flag_armable ) node->addSubscriber( "solo/info/armable" , &Virtual::armableCallback , this );
    if( flag_battery ) node->addSubscriber( "solo/info/battery" , &Virtual::batteryCallback , this );
    if( flag_gps     ) node->addSubscriber( "solo/info/gps"     , &Virtual::gpsCallback     , this );
}

const void
Virtual::startPublishers()
{
    pub_request = node->addPublisher< msgString >( "solo/command/request" );

    pub_takeoff = node->addPublisher< msgEmpty >( "solo/command/takeoff" );
    pub_landing = node->addPublisher< msgEmpty >( "solo/command/landing" );

    pub_location = node->addPublisher< msgVector3 >( "solo/command/location" );
    pub_velocity = node->addPublisher< msgVector3 >( "solo/command/velocity" );
    pub_bearing  = node->addPublisher< msgFloat32 >( "solo/command/bearing"  );
}

const void
Virtual::dataUpdate()
{
    node->publishString( pub_request , "SYSTEM_STATUS" );
    node->publishString( pub_request , "VEHICLE_MODE"  );
    node->publishString( pub_request , "ARMED_FLAG"    );
    node->publishString( pub_request , "ARMABLE_FLAG"  );
    node->publishString( pub_request , "GPS_LOCK"      );
}

const void
Virtual::drawDrone()
{
    draw->object( *drone );
    draw->lwc(2,GRE).line3D( poses );
    draw->lwc(2,BLU).line3D( goals ).line3D( goals[-1] , target );

    forLOOPi( goals.n() )
        draw->psc( 9 , i < cnt ? BLU : i == cnt ? CYA : MAG ).pt3D( goals[i] );
    draw->psc(9,RED).pt3D( target );
}

const void
Virtual::drawText()
{
    draw->flatten();

    // GLOBAL

    draw->clr(YEL).text2D( 0.01 , 0.95 , "Latitude :" );
    draw->clr(WHI).text2D( 0.15 , 0.95 , std::to_string( loc_global.x ) );

    draw->clr(YEL).text2D( 0.01 , 0.90 , "Longitude :" );
    draw->clr(WHI).text2D( 0.15 , 0.90 , std::to_string( loc_global.y ) );

    draw->clr(YEL).text2D( 0.01 , 0.85 , "Altitude :" );
    draw->clr(WHI).text2D( 0.15 , 0.85 , std::to_string( loc_global.z ) );

    draw->clr(YEL).text2D( 0.01 , 0.80 , "Heading :" );
    draw->clr(WHI).text2D( 0.15 , 0.80 , std::to_string( heading.data ) );

    // POSE

    draw->clr(YEL).text2D( 0.02 , 0.70 , "X :" );
    draw->clr(WHI).text2D( 0.10 , 0.70 , std::to_string( drone->pose->x() ) );

    draw->clr(YEL).text2D( 0.02 , 0.65 , "Y :" );
    draw->clr(WHI).text2D( 0.10 , 0.65 , std::to_string( drone->pose->y() ) );

    draw->clr(YEL).text2D( 0.02 , 0.60 , "Z :" );
    draw->clr(WHI).text2D( 0.10 , 0.60 , std::to_string( drone->pose->z() ) );

    draw->clr(CYA).text2D( 0.22 , 0.70 , "( " + std::to_string( goals[-1].x ) + " )" );
    draw->clr(CYA).text2D( 0.22 , 0.65 , "( " + std::to_string( goals[-1].y ) + " )" );
    draw->clr(CYA).text2D( 0.22 , 0.60 , "( " + std::to_string( goals[-1].z ) + " )" );

    if( goals.n() > 1 )
    {
        float d = distance();
        draw->clr( d > 2.0 ? RED : GRE ).text2D( 0.22 , 0.52 , "( " + std::to_string( d ) + " )" );
    }

    draw->clr(YEL).text2D( 0.02 , 0.55 , "R :" );
    draw->clr(WHI).text2D( 0.10 , 0.55 , std::to_string( drone->pose->r() ) );

    draw->clr(YEL).text2D( 0.02 , 0.50 , "P :" );
    draw->clr(WHI).text2D( 0.10 , 0.50 , std::to_string( drone->pose->p() ) );

    draw->clr(YEL).text2D( 0.02 , 0.45 , "W :" );
    draw->clr(WHI).text2D( 0.10 , 0.45 , std::to_string( drone->pose->w() ) );

    draw->clr(YEL).text2D( 0.02 , 0.35 , "VX :" );
    draw->clr(WHI).text2D( 0.10 , 0.35 , std::to_string( velocity.x ) );

    draw->clr(YEL).text2D( 0.02 , 0.30 , "VY :" );
    draw->clr(WHI).text2D( 0.10 , 0.30 , std::to_string( velocity.y ) );

    draw->clr(YEL).text2D( 0.02 , 0.25 , "VZ :" );
    draw->clr(WHI).text2D( 0.10 , 0.25 , std::to_string( velocity.z ) );

    // STATUS
    draw->clr(YEL).text2D( 0.01 , 0.03 , "STATUS :" );
    draw->clr(RED).text2D( 0.15 , 0.03 , status.data );

    // MODE
    draw->clr(YEL).text2D( 0.31 , 0.03 , "MODE :" );
    draw->clr(RED).text2D( 0.45 , 0.03 , mode.data );

    // ARMED
    draw->clr(YEL).text2D( 0.61 , 0.03 , "ARMED :" );
    if( armable.data ) draw->clr(GRE).text2D( 0.75 , 0.03 , isArmed() ? "TRUE" : "FALSE" );
    else               draw->clr(RED).text2D( 0.75 , 0.03 , isArmed() ? "TRUE" : "FALSE"  );

    // BATTERY
    draw->clr(YEL).text2D( 0.01 , 0.08 , "BATTERY :" );
    if( battery.data > 70 )
         draw->clr(GRE).text2D( 0.17 , 0.08 , std::to_string( battery.data ) + " %" );
    else if( battery.data > 40 )
         draw->clr(CYA).text2D( 0.17 , 0.08 , std::to_string( battery.data ) + " %" );
    else draw->clr(RED).text2D( 0.17 , 0.08 , std::to_string( battery.data ) + " %" );

    // GPS_LOCK
    draw->clr(YEL).text2D( 0.31 , 0.08 , "GPS LOCK :" );
    if( gps.x < 2 )
         draw->clr(RED).text2D( 0.47 , 0.08 , std::to_string( (int)gps.y ) );
    else if( gps.x == 2 )
         draw->clr(CYA).text2D( 0.47 , 0.08 , std::to_string( (int)gps.y ) );
    else draw->clr(GRE).text2D( 0.47 , 0.08 , std::to_string( (int)gps.y ) );

    // Poses
    draw->clr(YEL).text2D( 0.01 , 0.15 , "NUM. POSES :" );
    draw->clr(WHI).text2D( 0.20 , 0.15 , std::to_string( poses.n() ) );

    // Targets
    draw->clr(YEL).text2D( 0.31 , 0.15 , "NUM. TARGETS :" );
    draw->clr(WHI).text2D( 0.55 , 0.15 , std::to_string( cnt ) + " / " + std::to_string( goals.n() - 1 ) );

    // Control Type
    draw->clr(YEL).text2D( 0.78 , 0.95 , "CONTROL_TYPE" );
    draw->clr(GRE).text2D( 0.81 , 0.91 , control_type == LOCATION ? "LOCATION" : "VELOCITY" );

}

const void
Virtual::stateUpdate()
{
    mut_loc_local.lock();
        drone->pose->setPos( loc_local.x , -loc_local.y , - loc_local.z );
    mut_loc_local.unlock();

    mut_attitude.lock();
        drone->pose->setOrient( attitude.x , - attitude.y , - attitude.z );
    mut_attitude.unlock();

    if( isArmed() && !hasStarted() )
        offset = *drone->pose , started = true;

    *drone->pose -= offset;
    drone->pose->rotatePos( offset.R() );

    if( isArmed() && hasStarted() )
        poses.push( drone->pose->getPosPt() );

    new_state = false;
}

const void
Virtual::runDraw()
{
    draw = new CPPlot( "Window" , 2 , 2 );
    draw->useScreen(0).set3Dworld().setViewer( -4.0 , 0.0 , 2.0 );

    drone = new Drone( 1.0 );
    drone->setPos( 0.0 , 0.0 , 0.0 );

    Img3u img( "data/gandalf.jpg" );
    unsigned tex = draw->addTexture( img );

    draw->useScreen(1).set3Dworld().setViewer( drone->pose );

    Img3u frame;
    draw->useScreen(2).set2Dimage();

//    draw->store( frame );
    unsigned frm = draw->addTexture( frame );

    Borders3f borders;
    borders[3] = Pt3f( 3.0 , +1.0 , +1.0 );
    borders[0] = Pt3f( 3.0 , -1.0 , +1.0 );
    borders[1] = Pt3f( 3.0 , -1.0 , -1.0 );
    borders[2] = Pt3f( 3.0 , +1.0 , -1.0 );

    while( draw->input() && running )
    {
        if( new_state )
            stateUpdate();

        draw->useScreen(0).clear();

        draw->psc(6,BLU).pts3D( borders.points() );
        draw->useTexture( tex , borders );

        drawDrone(); drawText();

        draw->useScreen(1).clear();

        draw->psc(6,BLU).pts3D( borders.points() );
        draw->useTexture( borders );

//        draw->store( frame );
        draw->useScreen(2).clear().useTexture( frm , frame );

        draw->updateWindow(30);

        if( draw->keys.space )
        {
            if( isArmed() )
                 node->publishEmpty( pub_landing );
            else node->publishEmpty( pub_takeoff );
            halt(100);
        }

        if( isArmed() )
        {
            if( draw->keys.tab )
            {
                control_type = ++control_type % 2;
                if( control_type == LOCATION ) cnt-- , gotoLocation();
                halt(100);
            }

            switch( control_type )
            {
            case LOCATION: controlLocation( draw ); break;
            case VELOCITY: controlVelocity( draw ); break;
            }
        }

        if( draw->keys.j ) borders.points().mat().c(1) += 0.1;
        if( draw->keys.l ) borders.points().mat().c(1) -= 0.1;
        if( draw->keys.i ) borders.points().mat().c(2) += 0.1;
        if( draw->keys.k ) borders.points().mat().c(2) -= 0.1;
    }

    running = false;

    delete drone;
    delete draw;
}

const void
Virtual::runData()
{
    node = new ROSnode("Telemetry");

    startSubscribers();
    startPublishers();

    while( running && ros::ok() )
    {
        ros::spinOnce();

        if( mode.data == "NONE" )
        {
            dataUpdate();
            halt(100);
        }
    }

    running = false;

    delete node;
}

const void
Virtual::start()
{
    running = true;
    threadData = Thread( &Virtual::runData , this );
    runDraw();

    threadData.join();
}

}
