#include "./solo.h"

namespace cvpp
{

void // Callback Location Global
Solo::callbackGlobal( const msgVector3::ConstPtr& msg )
{
    mut_global.lock();

    gpos = Pt3f( msg->x , msg->y , msg->z );
    if( gfirst ) { gorg = gpos; gfirst = false; }
//    gpos -= gorg;

//    gdrone->setPos( gpos.x * 1.113195e5 ,
//                    gpos.y * 1.113195e5 , gpos.z );
//    if( flying ) gpath.push( gdrone->pose->getPosPt() );

    mut_global.unlock();
}


void // Callback Location Local
Solo::callbackLocal( const msgVector3::ConstPtr& msg )
{
    mut_local.lock();

    lpos = Pt3f( msg->x , msg->y , msg->z );
    if( lfirst ) { lorg = lpos; lfirst = false; }
    lpos -= lorg;

    ldrone->setPos( lpos.x ,
                    lpos.y , lpos.z );
    if( recording ) lpath.push( ldrone->pose->getPosPt() );

    mut_local.unlock();
}

void // Callback Attitude
Solo::callbackAttitude( const msgVector3::ConstPtr& msg )
{
    mut_attitude.lock();
    if( afirst ) { afirst = false; }

    ldrone->pose->setOrient( msg->x , msg->y , msg->z );

    mut_attitude.unlock();
}


void // Mode
Solo::callbackMode( const msgString::ConstPtr& msg )
{
    mode = msg->data;
}

void // Status
Solo::callbackStatus( const msgString::ConstPtr& msg )
{
    status = msg->data;
}

void // Armed
Solo::callbackArmed( const msgBool::ConstPtr& msg )
{
    armed = msg->data;
}

void // Armable
Solo::callbackArmable( const msgBool::ConstPtr& msg )
{
    armable = msg->data;
}

void // Battery
Solo::callbackBattery( const msgInt32::ConstPtr& msg )
{
    battery = msg->data;
}

void // GPS
Solo::callbackGPS( const msgVector3::ConstPtr& msg )
{
    gps.x = msg->x; gps.y = msg->y;
}

void // Image
Solo::callbackImage( const sensor_msgs::ImageConstPtr& msg )
{
    raw >> cv_bridge::toCvShare( msg , "bgr8" )->image;
    img = raw.resize( 480 , 640 );
    new_img = true;
}

}

