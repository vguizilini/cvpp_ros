#ifndef SOLO_H
#define SOLO_H

#include <cvpp/ros/ros_node.h>
#include <cvpp/interfaces/cpplot.h>
#include <cvpp/objects/object_drone.h>

#include <ros/ros.h>

#include <std_msgs/Int32.h>
#include <std_msgs/Float32.h>
#include <std_msgs/String.h>
#include <std_msgs/Bool.h>
#include <std_msgs/Empty.h>
#include <geometry_msgs/Vector3.h>

#define LOCATION 0
#define VELOCITY 1

using msgInt32   = std_msgs::Int32;
using msgFloat32 = std_msgs::Float32;
using msgString  = std_msgs::String;
using msgBool    = std_msgs::Bool;
using msgEmpty   = std_msgs::Empty;
using msgVector3 = geometry_msgs::Vector3;

namespace cvpp
{

class Solo
{

protected:

    ROSnode *node;
    Drone *gdrone,*ldrone;

    msgVector3 goal;
    Pts3f gpath,lpath,goals;
    Pt3f target,gorg,lorg,gpos,lpos,gps;

    Img3u raw,img;
    bool new_img;

    int battery;
    bool armed,armable;
    String mode,status;

    int cnt;
    bool running,flying,recording,control;
    bool gfirst,lfirst,afirst;

    Mutex mut_global,mut_local,mut_attitude;

    unsigned pub_takeoff,pub_landing;
    unsigned pub_location,pub_request;

public:

    Solo();
    ~Solo();

    const void start();

    const void runData();
    const void runDraw();

    const void checkTarget();
    const void requestUpdate();

    const void startSubscribers();
    const void startPublishers();

    const void addMission();

    const void drawText( CPPlot& , Drone& );

    void callbackGlobal( const msgVector3::ConstPtr& );
    void callbackLocal( const msgVector3::ConstPtr& );
    void callbackAttitude( const msgVector3::ConstPtr& );

    void callbackMode( const msgString::ConstPtr& );
    void callbackStatus( const msgString::ConstPtr& );
    void callbackArmed( const msgBool::ConstPtr& );
    void callbackArmable( const msgBool::ConstPtr& );
    void callbackBattery( const msgInt32::ConstPtr& );
    void callbackGPS( const msgVector3::ConstPtr& );

    void callbackImage( const sensor_msgs::ImageConstPtr& );

    const void publishNextGoal();

    Pt3f ned2lla( const Pt3f& ) const;
    Pt3f lla2ned( const Pt3f& ) const;

};

}

#endif
