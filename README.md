## CVPP_ROS  -  Computer Vision for C++ (ROS) ##
### by Vitor Campanholo Guizilini ###

---------------------------------------------------------------------------------------  
# *Installation*   
---------------------------------------------------------------------------------------

This is a ROS extension of the CVPP library. It was tested on Ubuntu 16.04, using ROS Kinetic, but it should compile fine in other distributions. 

From a fresh installation, do the following:
 
### Install the CVPP Library##

[https://bitbucket.org/vguizilini/cvpp](Link URL)

### Install ROS 

##### SETUP SOURCES

```
sudo sh -c 'echo "deb http://packages.ros.org/ros/ubuntu $(lsb_release -sc) main" > /etc/apt/sources.list.d/ros-latest.list'
```

##### SETUP KEYS

```
sudo apt-key adv --keyserver hkp://ha.pool.sks-keyservers.net:80 --recv-key 421C365BD9FF1F717815A3895523BAEEB01FA116
```

##### INSTALL PACKAGES


```
sudo apt-get update 
sudo apt-get install ros-kinetic-desktop-full
```

##### INITIALIZE ROSDEP

```
sudo rosdep init
rosdep update
```

##### EDIT .BASHRC

```
gedit ~/.bashrc
source /opt/ros/kinetic/setup.bash
```

### Install the CVPP_ROS Library ###

##### CREATE WORKSPACE

```
cd /path/to/catkin_folder
mkdir -p catkin/src
cd catkin
catkin_make
```

##### CLONE CVPP_ROS REPOSITORY

```
cd src
git clone https://bitbucket.org/vguizilini/cvpp_ros
```

##### SET CVPP PATH

```
gedit cvpp_ros/CVPP_ROS.cmake
(modify: CMAKE_MODULE_PATH ${CMAKE_MODULE_PATH} "~/path/to/cvpp/cmake" )
```

##### COMPILE WORKSPACE ###

```
cd ..
catkin_make
```

##### EDIT .BASHRC

```
gedit ~/.bashrc
source /path/to/catkin/devel/setup.bash
```

### Test Installation ###

```
rosrun cvpp_ros cam2scr 0
```

If it worked, you should see a screen showing the output of /dev/video0. 


