
#include <cvpp/ros/ros_node.h>
#include <cvpp/interfaces/cpplot.h>
#include <cvpp/objects/object_drone.h>

#include <nav_msgs/Odometry.h>

using namespace cvpp;

using msgOdom = nav_msgs::Odometry;

class Interface
{

protected:

    ROSnode* node;

    Drone* drone_odometry;
    bool new_odometry;
    Pts3f traj_odometry;

    Drone* drone_slam;
    bool new_slam , first_new_slam; Matf pose_slam;
    float offx , offy , offz , offr , offp , offw;
    Pts3f traj_slam;

    Img3c raw , frame;
    bool new_frame;

    int cnt_start;

    bool running;

public:

    Interface()
    {
        node = new ROSnode( "ORB_SLAM_INTERFACE" );

        node->addSubscriber( "/bebop/odom" , &Interface::callbackOdometry , this );
        node->addSubscriber( "/orb_slam2/pose" , &Interface::callbackSLAM , this );

        node->addImageSubscriber( "/bebop/image_raw" , &Interface::callbackImage, this );
    }

    ~Interface()
    {
        delete node;
    }

    void callbackOdometry( const msgOdom& msg )
    {
        drone_odometry->pose->setPos( msg.pose.pose.position.x ,
                                      msg.pose.pose.position.y ,
                                      msg.pose.pose.position.z );

        drone_odometry->pose->setOrient( msg.pose.pose.orientation.w ,
                                         msg.pose.pose.orientation.x ,
                                         msg.pose.pose.orientation.y ,
                                         msg.pose.pose.orientation.z );

        new_odometry = true;
    }

    void callbackSLAM( const msgMatf& msg )
    {
        pose_slam.fromROSmsg( msg );
        drone_slam->pose->setPose( pose_slam );

        float x = drone_slam->pose->x();
        float y = drone_slam->pose->y();
        float z = drone_slam->pose->z();
        float r = drone_slam->pose->r();
        float p = drone_slam->pose->p();
        float w = drone_slam->pose->w();

        if( !first_new_slam )
        {
            offx = x; offy = y; offz = z;
            offr = r; offp = p; offw = w;
            first_new_slam = true;
        }

        x -= offx; y -= offy; z -= offz;
        r -= offr; p -= offp; w -= offw;

        drone_slam->pose->setPos( - z + drone_odometry->pose->x() ,
                                    x + drone_odometry->pose->y() ,
                                    y + drone_odometry->pose->z() );

        drone_slam->pose->setOrient( - w + drone_odometry->pose->r() ,
                                       r + drone_odometry->pose->p() ,
                                       p + drone_odometry->pose->w() );

        new_slam = true;
    }

    void callbackImage( const msgImg3c& msg )
    {
        raw >> cv_bridge::toCvShare( msg , "bgr8" )->image;
        frame = raw.resize( 480 , 640 );
        new_frame = true;
    }

    void drawText( CPPlot& draw )
    {
        draw.flatten();

        Scalar clr = YEL;

        draw.clr(clr).text2D( 0.02 , 0.70 , "X :" );
        draw.clr(WHI).text2D( 0.10 , 0.70 , std::to_string( drone_odometry->pose->x() ) );

        draw.clr(clr).text2D( 0.02 , 0.65 , "Y :" );
        draw.clr(WHI).text2D( 0.10 , 0.65 , std::to_string( drone_odometry->pose->y() ) );

        draw.clr(clr).text2D( 0.02 , 0.60 , "Z :" );
        draw.clr(WHI).text2D( 0.10 , 0.60 , std::to_string( drone_odometry->pose->z() ) );

        draw.clr(clr).text2D( 0.02 , 0.55 , "R :" );
        draw.clr(WHI).text2D( 0.10 , 0.55 , std::to_string( drone_odometry->pose->r() ) );

        draw.clr(clr).text2D( 0.02 , 0.50 , "P :" );
        draw.clr(WHI).text2D( 0.10 , 0.50 , std::to_string( drone_odometry->pose->p() ) );

        draw.clr(clr).text2D( 0.02 , 0.45 , "W :" );
        draw.clr(WHI).text2D( 0.10 , 0.45 , std::to_string( drone_odometry->pose->w() ) );
    }

    void drawDrone( CPPlot& draw )
    {
        draw.object( drone_odometry );

        if( cnt_start > 0 )
        {
            draw.lwc(1,CYA).line3D( traj_odometry.n( 0 , cnt_start ) );
            draw.lwc(1,GRE).line3D( traj_odometry.n( cnt_start , traj_odometry.n() - cnt_start ) );
        }
        else
        {
            draw.lwc(1,GRE).line3D( traj_odometry );
        }

        draw.object( drone_slam );
        draw.lwc(1,RED).line3D( traj_slam );
    }

    void runCalls()
    {
        while( running )
        {
            node->spin();
            halt(1);
        }
    }

    void run()
    {
        running = true;

        CPPlot draw( "Window" , ULHW( 600 , 800 * 2 ) , 1 , 2 );
        draw[0].set3Dworld().setViewer(  -2.35584 , -0.231931 , 1.336430 ,
                                         -1.42372 , -0.117610 , 0.992809 );
        draw[1].set2Dimage();

        int tex_frame = draw.addTexture3U( 480 , 640 );
        new_frame = false;

        drone_odometry = new Drone( 0.25 );
        new_odometry = false;
        cnt_start = 0;

        drone_slam = new Drone( 0.25 );
        new_slam = first_new_slam = false;

        Thread calls( &Interface::runCalls , this );

        while( draw.input() && running )
        {
            if( draw.keys.space )
            {
                traj_slam.reset();
                cnt_start = traj_odometry.n();
                first_new_slam = false;
            }

            if( new_odometry )
            {
                traj_odometry.push( drone_odometry->pose->getPosPt() );
                new_odometry = false;
            }

            if( new_slam )
            {
                traj_slam.push( drone_slam->pose->getPosPt() );
                new_slam = false;
            }

            if( new_frame )
            {
                draw.updTexture( tex_frame , frame );
                new_frame = false;
            }

            draw[0].clear();

            drawDrone( draw );
            drawText( draw );

            draw[1].clear();

            draw.useTexture( tex_frame );

            draw.updateWindow(30);
        }

        running = false;
        calls.join();
    }
};

int main()
{
    Interface interface;
    interface.run();
}

