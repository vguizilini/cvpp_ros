
#include <cvpp/ros/ros_node.h>

#include <cvpp/interfaces/cpplot.h>
#include <cvpp/interfaces/cppture.h>

using namespace cvpp;

class vid2top2vid
{

protected:

    Img3c raw,ipub,isub;
    bool update;

public:

    void imageCallback( const sensor_msgs::ImageConstPtr& msg )
    {
        isub >> cv_bridge::toCvShare( msg , "bgr8" )->image;
        update = true;
    }

    void run()
    {
        CPPture cap( "data/bullfinch.mp4" );
        Dims dims( 480 , 640 );

        ROSnode node("Image");

        node.addImageSubscriber( "/camera/image" , &vid2top2vid::imageCallback , this );
        unsigned pub_image = node.addImagePublisher( "/camera/image" );

        CPPlot draw("Window",1,2);
        draw[0].set2Dimage();
        draw[1].set2Dimage();

        unsigned tex_pub = draw.addTexture3U( dims );
        unsigned tex_sub = draw.addTexture3U( dims );

        while( draw.input() && cap.read( raw ) )
        {
            ipub = raw.resize( dims );

            draw[0].clear().useTexture( tex_pub , ipub );
            node.publishImage( pub_image , ipub.cv() );

            if( update )
            {
                draw[1].clear().useTexture( tex_sub , isub );
                update = false;
            }

            draw.updateWindow(30);
            node.spin();
        }
    }

};

int main( int argc , char* argv[] )
{
    vid2top2vid code;
    code.run();

    return 0;
}


