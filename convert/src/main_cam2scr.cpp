
#include <cvpp/ros/convert/ros_cam2scr.h>

using namespace cvpp;

int main( int argc , char* argv[] )
{
    cam2scr code;
    code.run( std::atoi( argv[1] ) );

    return 0;
}

